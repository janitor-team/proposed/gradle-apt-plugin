Source: gradle-apt-plugin
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Miroslav Kravec <kravec.miroslav@gmail.com>
Build-Depends:
 debhelper (>= 11),
 default-jdk,
 gradle-debian-helper (>=1.4),
 maven-repo-helper
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/java-team/gradle-apt-plugin.git
Vcs-Browser: https://salsa.debian.org/java-team/gradle-apt-plugin
Homepage: https://github.com/tbroyer/gradle-apt-plugin

Package: gradle-apt-plugin
Architecture: all
Depends: ${misc:Depends}
Description: Gradle plugin to use Java annotation processors
 This plugin does a few things to make it easier/safer to use Java annotation
 processors in a Gradle build:
 .
   * it ensures the presence of configurations for your compile-time only
     dependencies (annotations, generally) and annotation processors,
     consistently across all supported Gradle versions;
   * automatically configures the corresponding JavaCompile and GroovyCompile
     tasks to make use of these configurations, when the java or groovy plugin
     is applied;
   * automatically configures IntelliJ IDEA and/or Eclipse when the
     net.ltgt.apt-idea or net.ltgt.apt-eclipse plugins are applied.
